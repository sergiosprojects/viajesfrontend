import { TestBed } from '@angular/core/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';

import { CityService } from './city.service';
import { City } from '../models/city.model';
import { environment } from 'src/environments/environment';

describe('CityService', () => {
  let service: CityService;
  let httpMock: HttpTestingController;
  let city: City;
  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [CityService]
    });
    service = TestBed.inject(CityService);
    httpMock = TestBed.inject(HttpTestingController);
    city = {
      id: 1,
      name: 'Medellín',
      amountOfPeople: 1000,
      mostVisitedSite: 'parque',
      mostReservedHotel: 'hotel',
    };
  });

  afterEach(() => {
    httpMock.verify();
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('getAll() should return data', () => {
    service.getAll().subscribe((res) => {
      expect(res).toEqual([city]);
    });

    const req = httpMock.expectOne(`${environment.baseUrl}ciudades`);
    expect(req.request.method).toBe('GET');
    req.flush([city]);
  });

  it('getById() should return data', () => {
    service.getById(1).subscribe((res) => {
      expect(res).toEqual(city);
    });

    const req = httpMock.expectOne(`${environment.baseUrl}ciudades/1`);
    expect(req.request.method).toBe('GET');
    req.flush(city);
  });

  it('save() should POST and return data', () => {
    service.save(city).subscribe((res) => {
      expect(res).toEqual(city);
    });

    const req = httpMock.expectOne(`${environment.baseUrl}ciudades`);
    expect(req.request.method).toBe('POST');
    req.flush(city);
  });

  it('deleteById() should DELETE and return ok', () => {
    service.deleteById(1).subscribe((res) => {
      expect(res).toEqual('ok');
    });

    const req = httpMock.expectOne(`${environment.baseUrl}ciudades/delete/1`);
    expect(req.request.method).toBe('DELETE');
    req.flush('ok');
  });

  it('update() should PUT and return ok', () => {
    service.update(city).subscribe((res) => {
      expect(res).toEqual('ok');
    });

    const req = httpMock.expectOne(`${environment.baseUrl}ciudades/update`);
    expect(req.request.method).toBe('PUT');
    req.flush('ok');
  });
});
