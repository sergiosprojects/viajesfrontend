/* eslint-disable no-empty-function */
/* eslint-disable no-unused-vars */
/* eslint-disable import/no-unresolved */
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { City } from '../models/city.model';

@Injectable({
  providedIn: 'root',
})
export class CityService {
  private API_SERVER: string = `${environment.baseUrl}ciudades`;

  constructor(private http: HttpClient) {}

  public getAll(): Observable<City[]> {
    return this.http.get<City[]>(this.API_SERVER);
  }

  public getById(id: number): Observable<City> {
    return this.http.get<City>(`${this.API_SERVER}/${id}`);
  }

  public save(city: City): Observable<City> {
    return this.http.post<City>(this.API_SERVER, city);
  }

  public deleteById(id: number): Observable<string> {
    return this.http.delete<string>(`${this.API_SERVER}/delete/${id}`);
  }

  public update(city: City): Observable<string> {
    return this.http.put<string>(`${this.API_SERVER}/update`, city);
  }
}
