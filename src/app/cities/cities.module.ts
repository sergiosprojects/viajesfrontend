import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HttpClientModule } from '@angular/common/http';
import { CityComponent } from './components/city/city.component';
import { CityFormComponent } from './components/city-form/city-form.component';
import { CitiesRoutingModule } from './cities-routing.module';
import { CityService } from './services/city.service';
import { SharedModule } from '../shared/shared.module';
import { DeleteCityComponent } from './components/delete-city/delete-city.component';

@NgModule({
  declarations: [CityComponent, CityFormComponent, DeleteCityComponent],
  imports: [CommonModule, HttpClientModule, CitiesRoutingModule, SharedModule],
  exports: [],
  providers: [
    CityService,
  ],
})
export class CitiesModule {}
