/* eslint-disable no-empty-function */
/* eslint-disable no-unused-vars */
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { CityService } from '../../services/city.service';

@Component({
  selector: 'app-city-form',
  templateUrl: './city-form.component.html',
  styleUrls: ['./city-form.component.css'],
})
export class CityFormComponent implements OnInit {
  cityForm!: FormGroup;

  cityId: any;

  constructor(
    private cityService: CityService,
    private formBuilder: FormBuilder,
    private route: ActivatedRoute,
    private router: Router,
    private toastrService: ToastrService
  ) {}

  ngOnInit(): void {
    this.cityForm = this.formBuilder.group({
      id: [],
      name: ['', Validators.required],
      amountOfPeople: ['', Validators.required],
      mostVisitedSite: ['', Validators.required],
      mostReservedHotel: ['', Validators.required],
    });
    this.loadData();
  }

  action() {
    if (this.cityId) {
      this.edit();
    } else {
      this.save();
    }
  }

  save() {
    this.cityService.save(this.cityForm.value).subscribe(
      () => {
        this.cityForm.reset();
        this.savedMessage();
      },
      (error) => console.error(error)
    );
  }

  edit() {
    this.cityService.update(this.cityForm.value).subscribe(
      () => {
        this.savedMessage();
        this.router.navigate(['ciudades']);
      },
      (error) => console.error(error)
    );
  }

  loadData(): void {
    this.cityId = this.route.snapshot.paramMap.get('id');
    if (this.cityId) {
      this.cityService.getById(this.cityId).subscribe(
        (resp) => {
          this.cityForm.setValue({
            id: resp.id,
            name: resp.name,
            amountOfPeople: resp.amountOfPeople,
            mostVisitedSite: resp.mostVisitedSite,
            mostReservedHotel: resp.mostReservedHotel,
          });
        },
        (error) => {
          console.error(error);
          this.router.navigate(['not-found']);
        }
      );
    }
  }

  savedMessage() {
    this.toastrService.success('Se han guardado los datos exitosamente', 'OK');
  }
}
