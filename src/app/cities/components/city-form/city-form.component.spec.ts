import { ComponentFixture, TestBed } from '@angular/core/testing';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { By } from '@angular/platform-browser';
import { ActivatedRoute, convertToParamMap, Router } from '@angular/router';
import { ToastrModule, ToastrService } from 'ngx-toastr';
import { of } from 'rxjs';
import { City } from '../../models/city.model';
import { CityService } from '../../services/city.service';

import { CityFormComponent } from './city-form.component';

describe('CityFormComponent', () => {
  let component: CityFormComponent;
  let fixture: ComponentFixture<CityFormComponent>;
  let serviceSpy: jasmine.SpyObj<CityService>;
  let routerSpy: jasmine.SpyObj<Router>;
  let city: City;

  beforeEach(async () => {
    serviceSpy = jasmine.createSpyObj<CityService>('CityService', [
      'getById',
      'save',
      'update',
    ]);
    routerSpy = jasmine.createSpyObj<Router>('Router', ['navigate']);
    serviceSpy.getById.and.returnValue(of(city));
    await TestBed.configureTestingModule({
      declarations: [CityFormComponent],
      imports: [ReactiveFormsModule, FormsModule, ToastrModule],
      providers: [
        { provide: ToastrService, useValue: () => {} },
        { provide: CityService, useValue: serviceSpy },
        { provide: Router, useValue: routerSpy },
        {
          provide: ActivatedRoute,
          useValue: {
            snapshot: { paramMap: convertToParamMap({ id: '1' }) },
          },
        },
      ],
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CityFormComponent);
    component = fixture.componentInstance;
    city = {
      id: 1,
      name: 'Medellín',
      amountOfPeople: 1000,
      mostVisitedSite: 'parque',
      mostReservedHotel: 'hotel',
    };
    serviceSpy.getById.and.returnValue(of(city));
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('loadData function should load data', () => {
    component.loadData();
    expect(component.cityForm).toBeDefined();
  });

  it('action function have been called on submit', () => {
    spyOn(component, 'action').and.callFake(() => {});
    component.cityForm.setValue(city);

    fixture.debugElement.query(By.css('form'))
      .triggerEventHandler('ngSubmit', null);

    expect(component.action).toHaveBeenCalled();
  });

  it('when id exists, action function should call edit function', () => {
    spyOn(component, 'edit').and.callFake(() => {});

    component.cityForm.setValue(city);
    component.cityId = 1;
    component.action();

    expect(component.edit).toHaveBeenCalled();
  });

  it('when id not exists, action function should call save function', () => {
    spyOn(component, 'save').and.callFake(() => {});

    component.cityForm.setValue(city);
    component.cityId = undefined;
    component.action();

    expect(component.save).toHaveBeenCalled();
  });

  it('save function should save city', () => {
    serviceSpy.save.and.callFake(() => of(city));
    spyOn(component, 'savedMessage').and.callFake(() => {});
    component.cityForm.setValue(city);
    component.save();

    expect(component.savedMessage).toHaveBeenCalled();
  });

  it('edit function should edit city', () => {
    serviceSpy.update.and.callFake(() => of(''));
    spyOn(component, 'savedMessage').and.callFake(() => {});
    component.cityForm.setValue(city);
    component.edit();

    expect(component.savedMessage).toHaveBeenCalled();
  });
});
