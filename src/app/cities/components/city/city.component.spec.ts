import { NO_ERRORS_SCHEMA } from '@angular/core';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { Router } from '@angular/router';
import { NgxPaginationModule } from 'ngx-pagination';
import { of } from 'rxjs';
import { City } from '../../models/city.model';
import { CityService } from '../../services/city.service';

import { CityComponent } from './city.component';

describe('CityComponent', () => {
  let component: CityComponent;
  let fixture: ComponentFixture<CityComponent>;
  let city: City;
  let serviceSpy: jasmine.SpyObj<CityService>;
  let routerSpy: jasmine.SpyObj<Router>;
  beforeEach(async () => {
    serviceSpy = jasmine.createSpyObj<CityService>('CityService', ['getAll']);
    routerSpy = jasmine.createSpyObj<Router>('Router', ['navigate']);
    await TestBed.configureTestingModule({
      declarations: [CityComponent],
      imports: [NgxPaginationModule],
      providers: [
        { provide: Router, useValue: routerSpy },
        { provide: CityService, useValue: serviceSpy },
      ],
      schemas: [NO_ERRORS_SCHEMA],
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CityComponent);
    component = fixture.componentInstance;
    city = {
      id: 1,
      name: 'Medellín',
      amountOfPeople: 1000,
      mostVisitedSite: 'parque',
      mostReservedHotel: 'hotel',
    };
    serviceSpy.getAll.and.returnValue(of([city]));
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('data function should load data', () => {
    component.data();
    expect(component.cities.length).toEqual(1);
    fixture.detectChanges();
  });
});
