/* eslint-disable class-methods-use-this */
/* eslint-disable nonblock-statement-body-position */
/* eslint-disable curly */
/* eslint-disable no-empty-function */
/* eslint-disable no-unused-vars */
/* eslint-disable no-return-assign */
/* eslint-disable comma-dangle */
import { Component, OnInit } from '@angular/core';
import { City } from '../../models/city.model';
import { CityService } from '../../services/city.service';

@Component({
  selector: 'app-city',
  templateUrl: './city.component.html',
  styleUrls: ['./city.component.css'],
})
export class CityComponent implements OnInit {
  cities!: City[];

  p: number = 1;

  constructor(private cityService: CityService) {}

  ngOnInit(): void {
    this.data();
  }

  data() {
    this.cityService.getAll().subscribe(
      (resp) => (this.cities = resp),
      (error) => console.error(error)
    );
  }
}
