/* eslint-disable no-return-assign */
/* eslint-disable no-empty-function */
/* eslint-disable comma-dangle */
/* eslint-disable no-unused-vars */
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { City } from '../../models/city.model';
import { CityService } from '../../services/city.service';

@Component({
  selector: 'app-delete-city',
  templateUrl: './delete-city.component.html',
  styleUrls: ['./delete-city.component.css'],
})
export class DeleteCityComponent implements OnInit {
  city!: City;

  constructor(
    private cityService: CityService,
    private route: ActivatedRoute,
    private router: Router
  ) {}

  ngOnInit(): void {
    this.loadData();
  }

  loadData() {
    const cityId = Number(this.route.snapshot.paramMap.get('id'));
    this.cityService.getById(cityId).subscribe(
      (resp) => (this.city = resp),
      (error) => console.error(error)
    );
  }

  delete(id: number) {
    this.cityService.deleteById(id).subscribe(
      () => this.router.navigate(['ciudades']),
      (error) => console.error(error)
    );
  }
}
