import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute, convertToParamMap, Router } from '@angular/router';
import { of } from 'rxjs';
import { City } from '../../models/city.model';
import { CityService } from '../../services/city.service';

import { DeleteCityComponent } from './delete-city.component';

describe('DeleteCityComponent', () => {
  let component: DeleteCityComponent;
  let fixture: ComponentFixture<DeleteCityComponent>;
  let serviceSpy: jasmine.SpyObj<CityService>;
  let routerSpy: jasmine.SpyObj<Router>;

  beforeEach(async () => {
    serviceSpy = jasmine.createSpyObj<CityService>('CityService', [
      'getById',
      'deleteById',
    ]);
    routerSpy = jasmine.createSpyObj<Router>('Router', ['navigate']);
    await TestBed.configureTestingModule({
      declarations: [DeleteCityComponent],
      imports: [],
      providers: [
        { provide: Router, useValue: routerSpy },
        { provide: CityService, useValue: serviceSpy },
        {
          provide: ActivatedRoute,
          useValue: {
            snapshot: { paramMap: convertToParamMap({ id: '1' }) },
          },
        },
      ],
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DeleteCityComponent);
    component = fixture.componentInstance;
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('loadData function should load data', () => {
    const city: City = {
      id: 1,
      name: 'Medellín',
      amountOfPeople: 1000,
      mostVisitedSite: 'parque',
      mostReservedHotel: 'hotel',
    };
    serviceSpy.getById.and.returnValue(of(city));
    fixture.detectChanges();
    expect(component.city).toBeDefined();
  });

  it('delete function should delete item', () => {
    serviceSpy.deleteById.and.returnValue(of('ok'));
    component.delete(1);
    expect(routerSpy.navigate).toHaveBeenCalledWith(['ciudades']);
  });
});
