export interface City {
    id: number;
    name: string;
    amountOfPeople: number;
    mostVisitedSite: string;
    mostReservedHotel: string;
}