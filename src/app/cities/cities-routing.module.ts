import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AuthGuardService } from '../security/services/auth-guard.service';
import { CityFormComponent } from './components/city-form/city-form.component';
import { CityComponent } from './components/city/city.component';
import { DeleteCityComponent } from './components/delete-city/delete-city.component';

const routes: Routes = [
  { path: '', component: CityComponent, canActivate: [AuthGuardService] },
  { path: 'crear', component: CityFormComponent, canActivate: [AuthGuardService] },
  { path: 'editar/:id', component: CityFormComponent, canActivate: [AuthGuardService] },
  { path: 'eliminar/:id', component: DeleteCityComponent, canActivate: [AuthGuardService] },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class CitiesRoutingModule {}
