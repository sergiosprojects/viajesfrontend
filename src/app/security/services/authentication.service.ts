/* eslint-disable no-console */
/* eslint-disable class-methods-use-this */
/* eslint-disable prefer-template */
/* eslint-disable no-empty-function */
/* eslint-disable no-unused-vars */
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { map } from 'rxjs/operators';
import { environment } from 'src/environments/environment';
import { User } from '../models/user.model';

@Injectable({
  providedIn: 'root',
})
export class AuthenticationService {
  private API_SERVER: string = `${environment.baseUrl}auth`;

  user!: User;

  constructor(private http: HttpClient) {}

  authenticationService(user: User) {
    const headers = new HttpHeaders({
      Authorization: `Basic ${btoa(user.username + ':' + user.password)}`,
    });
    console.log(headers);
    return this.http
      .get<User>(`${this.API_SERVER}/basicauth`, { headers })
      .pipe(
        map((userData) => {
          sessionStorage.setItem('token', window.btoa(`${user.username}:${user.password}`));
          return userData;
        })
      );
  }

  isUserLoggedIn() {
    const token = sessionStorage.getItem('token');
    return token != null;
  }

  logout() {
    sessionStorage.removeItem('token');
  }
}
