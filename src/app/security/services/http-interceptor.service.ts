/* eslint-disable no-console */
/* eslint-disable prefer-template */
/* eslint-disable quote-props */
/* eslint-disable no-empty-function */
/* eslint-disable no-unused-vars */
import {
  HttpEvent,
  HttpHandler,
  HttpHeaders,
  HttpInterceptor,
  HttpRequest
} from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { AuthenticationService } from './authentication.service';

@Injectable({
  providedIn: 'root'
})
export class HttpInterceptorService implements HttpInterceptor {
  constructor(private authService: AuthenticationService) {}

  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    if (this.authService.isUserLoggedIn()) {
      const authReq = req.clone({
        headers: new HttpHeaders({
          'Content-Type': 'application/json',
          'Authorization': `Basic ${sessionStorage.getItem('token')}`
        })
      });
      return next.handle(authReq);
    }
    return next.handle(req);
  }
}
