/* eslint-disable no-restricted-globals */
/* eslint-disable no-empty-function */
/* eslint-disable no-unused-vars */
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { AuthenticationService } from '../../services/authentication.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css'],
})
export class LoginComponent implements OnInit {
  loginForm!: FormGroup;

  errorMessage = 'Credenciales invalidas';

  invalidLogin = false;

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private authService: AuthenticationService,
    private formBuilder: FormBuilder,
    private toastrService: ToastrService
  ) {}

  ngOnInit(): void {
    this.loginForm = this.formBuilder.group({
      username: ['', Validators.required],
      password: ['', Validators.required],
    });
  }

  handleLogin() {
    this.authService.authenticationService(this.loginForm.value).subscribe(
      () => {
        this.invalidLogin = false;
        this.router.navigate(['home'])
          .then(() => { location.reload(); });
      },
      (error: any) => {
        console.error(error);
        this.invalidLogin = true;
      }
    );
  }

  successMessage() {
    this.toastrService.success('Sesion iniciada correctamente', 'OK');
  }
}
