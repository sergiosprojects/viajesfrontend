import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AuthGuardService } from '../security/services/auth-guard.service';
import { DeleteTripComponent } from './components/delete-trip/delete-trip.component';
import { DetailsComponent } from './components/trip-details/trip-details.component';
import { TripFormComponent } from './components/trip-form/trip-form.component';
import { TripComponent } from './components/trip/trip.component';

const routes: Routes = [
  { path: '', component: TripComponent, canActivate: [AuthGuardService] },
  { path: 'crear', component: TripFormComponent, canActivate: [AuthGuardService] },
  { path: 'editar/:id', component: TripFormComponent, canActivate: [AuthGuardService] },
  { path: 'detalle/:id', component: DetailsComponent, canActivate: [AuthGuardService] },
  { path: 'eliminar/:id', component: DeleteTripComponent, canActivate: [AuthGuardService] },
  { path: ':opcion/:id', component: TripComponent, canActivate: [AuthGuardService] },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class TripsRoutingModule {}
