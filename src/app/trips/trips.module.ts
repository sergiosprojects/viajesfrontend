import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HttpClientModule } from '@angular/common/http';
import { TripComponent } from './components/trip/trip.component';
import { TripFormComponent } from './components/trip-form/trip-form.component';
import { TripsRoutingModule } from './trips-routing.module';
import { TripService } from './services/trip.service';
import { SharedModule } from '../shared/shared.module';
import { DetailsComponent } from './components/trip-details/trip-details.component';
import { DeleteTripComponent } from './components/delete-trip/delete-trip.component';

@NgModule({
  declarations: [TripComponent, TripFormComponent, DetailsComponent, DeleteTripComponent],
  imports: [CommonModule, HttpClientModule, TripsRoutingModule, SharedModule],
  exports: [],
  providers: [TripService],
})
export class TripsModule {}
