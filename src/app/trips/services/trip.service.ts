/* eslint-disable no-unused-vars */
/* eslint-disable no-empty-function */
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { Trip } from '../models/trip.model';

@Injectable({
  providedIn: 'root',
})
export class TripService {
  private API_SERVER: string = `${environment.baseUrl}viajes`;

  constructor(private http: HttpClient) {}

  public getAll(): Observable<Trip[]> {
    return this.http.get<Trip[]>(this.API_SERVER);
  }

  public getById(id: number): Observable<Trip> {
    return this.http.get<Trip>(`${this.API_SERVER}/${id}`);
  }

  public getAllByTourist(id: number): Observable<Trip[]> {
    return this.http.get<Trip[]>(`${this.API_SERVER}/turista/${id}`);
  }

  getAllByCity(id: number): Observable<Trip[]> {
    return this.http.get<Trip[]>(`${this.API_SERVER}/ciudad/${id}`);
  }

  save(trip: Trip): Observable<Trip> {
    return this.http.post<Trip>(this.API_SERVER, trip);
  }

  update(trip: Trip): Observable<string> {
    return this.http.put<string>(`${this.API_SERVER}/update`, trip);
  }

  deleteById(id: number): Observable<string> {
    return this.http.delete<string>(`${this.API_SERVER}/delete/${id}`);
  }
}
