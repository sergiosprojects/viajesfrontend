import { City } from 'src/app/cities/models/city.model';
import { Tourist } from 'src/app/tourists/models/tourist.model';

export interface Trip {
  id: number;
  date: Date;
  budget: number;
  creditCard: boolean;
  city: City;
  tourist: Tourist;
}
