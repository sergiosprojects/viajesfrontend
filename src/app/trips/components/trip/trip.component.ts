/* eslint-disable lines-between-class-members */
/* eslint-disable no-return-assign */
/* eslint-disable comma-dangle */
/* eslint-disable no-empty-function */
/* eslint-disable no-unused-vars */
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Trip } from '../../models/trip.model';
import { TripService } from '../../services/trip.service';

@Component({
  selector: 'app-trip',
  templateUrl: './trip.component.html',
  styleUrls: ['./trip.component.css'],
})
export class TripComponent implements OnInit {
  trips!: Trip[];

  p: number = 1;

  option!: any;

  optionId!: any;

  constructor(
    private tripService: TripService,
    private route: ActivatedRoute,
    private router: Router
  ) {}

  ngOnInit(): void {
    this.seeData();
  }

  seeData() {
    this.option = this.route.snapshot.paramMap.get('opcion');
    this.optionId = this.route.snapshot.paramMap.get('id');
    if (this.option && this.optionId) {
      switch (this.option) {
        case 'turista':
          this.getAllByTourist(this.optionId);
          break;
        case 'ciudad':
          this.getAllByCity(this.optionId);
          break;
        default:
          this.router.navigate(['viajes']);
      }
    } else {
      this.getAll();
    }
  }

  getAll() {
    this.tripService.getAll().subscribe(
      (resp) => (this.trips = resp),
      (error) => console.error(error)
    );
  }

  getAllByTourist(id: number) {
    this.tripService.getAllByTourist(id).subscribe(
      (resp) => (this.trips = resp),
      (error) => console.error(error)
    );
  }

  getAllByCity(id: number) {
    this.tripService.getAllByCity(id).subscribe(
      (resp) => (this.trips = resp),
      (error) => console.error(error)
    );
  }
}
