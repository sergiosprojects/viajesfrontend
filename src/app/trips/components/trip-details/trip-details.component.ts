/* eslint-disable comma-dangle */
/* eslint-disable no-empty-function */
/* eslint-disable no-unused-vars */
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Trip } from '../../models/trip.model';
import { TripService } from '../../services/trip.service';

@Component({
  selector: 'app-details',
  templateUrl: './trip-details.component.html',
  styleUrls: ['./trip-details.component.css'],
})
export class DetailsComponent implements OnInit {
  trip!: Trip;

  constructor(
    private tripService: TripService,
    private route: ActivatedRoute
  ) {}

  ngOnInit(): void {
    this.data();
  }

  data() {
    const tripId = Number(this.route.snapshot.paramMap.get('id'));
    if (tripId) {
      this.tripService.getById(tripId).subscribe(
        (resp) => {
          this.trip = resp;
        },
        (error) => {
          console.error(error);
        }
      );
    }
  }
}
