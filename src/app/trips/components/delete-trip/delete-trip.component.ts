/* eslint-disable no-unused-vars */
/* eslint-disable comma-dangle */
/* eslint-disable no-return-assign */
/* eslint-disable no-empty-function */
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Trip } from '../../models/trip.model';
import { TripService } from '../../services/trip.service';

@Component({
  selector: 'app-delete-trip',
  templateUrl: './delete-trip.component.html',
  styleUrls: ['./delete-trip.component.css'],
})
export class DeleteTripComponent implements OnInit {
  trip!: Trip;

  constructor(
    private tripService: TripService,
    private route: ActivatedRoute,
    private router: Router
  ) {}

  ngOnInit(): void {
    this.loadData();
  }

  loadData() {
    const tripId = Number(this.route.snapshot.paramMap.get('id'));
    this.tripService.getById(tripId).subscribe(
      (resp) => (this.trip = resp),
      (error) => console.error(error)
    );
  }

  delete(id: number) {
    this.tripService.deleteById(id).subscribe(
      () => this.router.navigate(['viajes']),
      (error) => console.error(error)
    );
  }
}
