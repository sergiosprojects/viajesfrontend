import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { City } from 'src/app/cities/models/city.model';
import { CityService } from 'src/app/cities/services/city.service';
import { Tourist } from 'src/app/tourists/models/tourist.model';
import { TouristService } from 'src/app/tourists/services/tourist.service';
import { TripService } from '../../services/trip.service';

@Component({
  selector: 'app-trip-form',
  templateUrl: './trip-form.component.html',
  styleUrls: ['./trip-form.component.css'],
})
export class TripFormComponent implements OnInit {
  tripForm!: FormGroup;

  tripId: any;

  sw: boolean = false;

  error: boolean = false;

  tourists!: Tourist[];

  cities!: City[];

  constructor(
    private tripService: TripService,
    private touristService: TouristService,
    private cityService: CityService,
    private toastrService: ToastrService,
    private formBuilder: FormBuilder,
    private route: ActivatedRoute,
    private router: Router
  ) {}

  ngOnInit(): void {
    this.tripForm = this.formBuilder.group({
      id: [''],
      city: ['', Validators.required],
      tourist: ['', Validators.required],
      date: ['', Validators.required],
      budget: ['', Validators.required],
      creditCard: ['', Validators.required],
    });

    this.getTourists();
    this.getCities();
    this.loadData();
  }

  action() {
    if (this.tripId) {
      this.edit();
    } else {
      this.save();
    }
  }

  save() {
    this.tripService.save(this.tripForm.value).subscribe(
      (resp) => {
        this.tripForm.reset();
        this.savedMessage();
      },
      (error) => {
        if (error.status == 405) {
          this.errorMessage();
        }
        console.error(error);
      }
    );
  }

  edit() {
    this.tripService.update(this.tripForm.value).subscribe(
      (resp) => {
        this.savedMessage();
        this.router.navigate(['viajes']);
      },
      (error) => console.error(error)
    );
  }

  getTourists() {
    this.touristService.getAll().subscribe(
      (resp) => (this.tourists = resp),
      (error) => console.error(error)
    );
  }

  getCities() {
    this.cityService.getAll().subscribe(
      (resp) => (this.cities = resp),
      (error) => console.error(error)
    );
  }

  loadData() {
    this.tripId = this.route.snapshot.paramMap.get('id');
    if (this.tripId) {
      this.tripService.getById(this.tripId).subscribe(
        (resp) => {
          this.tripForm.setValue({
            id: resp.id,
            city: resp.city,
            tourist: resp.tourist,
            date: resp.date,
            budget: resp.budget,
            creditCard: resp.creditCard,
          });
        },
        (error) => {
          console.error(error);
          this.router.navigate(['not-found']);
        }
      );
    }
  }

  savedMessage() {
    this.toastrService.success('Se han guardado los datos exitosamente', 'OK');
  }
  errorMessage() {
    this.toastrService.error(
      'Se ha incumplido alguna condicion y no se pudo guardar',
      'Error'
    );
  }
}
