/* eslint-disable no-unused-vars */
/* eslint-disable no-empty-function */
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { TouristService } from '../../services/tourist.service';

@Component({
  selector: 'app-tourist-form',
  templateUrl: './tourist-form.component.html',
  styleUrls: ['./tourist-form.component.css'],
})
export class TouristFormComponent implements OnInit {
  touristForm!: FormGroup;

  touristId: any;

  messages = { edit: false };

  constructor(
    private touristService: TouristService,
    private formBuilder: FormBuilder,
    private route: ActivatedRoute,
    private router: Router,
    private toastrService: ToastrService
  ) {}

  ngOnInit(): void {
    this.touristForm = this.formBuilder.group({
      id: ['', Validators.required],
      names: ['', Validators.required],
      surnames: ['', Validators.required],
      idType: ['', Validators.required],
      dateOfBirth: ['', Validators.required],
      travelFrequency: ['', Validators.required],
    });
    this.loadData();
  }

  action() {
    if (this.touristId) {
      this.edit();
    } else {
      this.save();
    }
  }

  save() {
    this.touristService.save(this.touristForm.value).subscribe(
      (resp) => {
        this.touristForm.reset();
        this.savedMessage();
      },
      (error) => {
        if (error.status === 400) {
          this.errorMessage(this.touristForm.value.id);
        }
        console.error(error);
      }
    );
  }

  edit() {
    this.touristService.update(this.touristForm.value).subscribe(
      (resp) => {
        this.savedMessage();
        this.router.navigate(['turistas']);
      },
      (error) => console.error(error)
    );
  }

  loadData(): void {
    this.touristId = this.route.snapshot.paramMap.get('id');
    if (this.touristId) {
      this.touristService.getById(this.touristId).subscribe(
        (resp) => {
          this.messages.edit = true;
          this.touristForm.setValue({
            id: resp.id,
            names: resp.names,
            surnames: resp.surnames,
            idType: resp.idType,
            dateOfBirth: resp.dateOfBirth,
            travelFrequency: resp.travelFrequency,
          });
        },
        (error) => {
          console.error(error);
          this.router.navigate(['not-found']);
        }
      );
    }
  }

  savedMessage() {
    this.toastrService.success('Se han guardado los datos exitosamente', 'OK');
  }

  errorMessage(id: number) {
    this.toastrService.error(`Ya existe un turista con el id: ${id}, Error`);
  }
}
