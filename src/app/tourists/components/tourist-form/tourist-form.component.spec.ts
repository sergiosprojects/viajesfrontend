import { ComponentFixture, TestBed } from '@angular/core/testing';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ActivatedRoute, convertToParamMap, Router } from '@angular/router';
import { ToastrModule, ToastrService } from 'ngx-toastr';
import { TouristService } from '../../services/tourist.service';

import { TouristFormComponent } from './tourist-form.component';

describe('TouristFormComponent', () => {
  let component: TouristFormComponent;
  let fixture: ComponentFixture<TouristFormComponent>;
  let serviceSpy: jasmine.SpyObj<TouristService>;
  let routerSpy: jasmine.SpyObj<Router>;
  beforeEach(async () => {
    serviceSpy = jasmine.createSpyObj<TouristService>('TouristService', [
      'getById',
      'save',
      'update',
    ]);
    routerSpy = jasmine.createSpyObj<Router>('Router', ['navigate']);
    await TestBed.configureTestingModule({
      declarations: [TouristFormComponent],
      imports: [ReactiveFormsModule, FormsModule, ToastrModule],
      providers: [
        { provide: ToastrService, useValue: () => {} },
        { provide: TouristService, useValue: serviceSpy },
        { provide: Router, useValue: routerSpy },
        {
          provide: ActivatedRoute,
          useValue: {
            snapshot: { paramMap: convertToParamMap({ id: '1' }) },
          },
        },
      ]
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(TouristFormComponent);
    component = fixture.componentInstance;
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
