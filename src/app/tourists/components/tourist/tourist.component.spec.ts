import { ComponentFixture, TestBed } from '@angular/core/testing';
import { Router } from '@angular/router';
import { NgxPaginationModule } from 'ngx-pagination';
import { TouristService } from '../../services/tourist.service';

import { TouristComponent } from './tourist.component';

describe('TouristComponent', () => {
  let component: TouristComponent;
  let fixture: ComponentFixture<TouristComponent>;
  let serviceSpy: jasmine.SpyObj<TouristService>;
  let routerSpy: jasmine.SpyObj<Router>;
  beforeEach(async () => {
    serviceSpy = jasmine.createSpyObj<TouristService>('CityService', ['getAll']);
    routerSpy = jasmine.createSpyObj<Router>('Router', ['navigate']);
    await TestBed.configureTestingModule({
      declarations: [TouristComponent],
      imports: [NgxPaginationModule],
      providers: [
        { provide: Router, useValue: routerSpy },
        { provide: TouristService, useValue: serviceSpy },
      ]
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(TouristComponent);
    component = fixture.componentInstance;
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
