/* eslint-disable no-empty-function */
/* eslint-disable no-unused-vars */
/* eslint-disable comma-dangle */
/* eslint-disable no-return-assign */
import { Component, OnInit } from '@angular/core';
import { Tourist } from '../../models/tourist.model';
import { TouristService } from '../../services/tourist.service';

@Component({
  selector: 'app-tourist',
  templateUrl: './tourist.component.html',
  styleUrls: ['./tourist.component.css'],
})
export class TouristComponent implements OnInit {
  tourists!: Tourist[];

  p: number = 1;

  constructor(private touristService: TouristService) {}

  ngOnInit(): void {
    this.data();
  }

  data() {
    this.touristService.getAll().subscribe(
      (resp) => (this.tourists = resp),
      (error) => console.error(error)
    );
  }
}
