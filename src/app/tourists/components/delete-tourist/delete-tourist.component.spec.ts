import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute, convertToParamMap, Router } from '@angular/router';
import { TouristService } from '../../services/tourist.service';

import { DeleteTouristComponent } from './delete-tourist.component';

describe('DeleteTouristComponent', () => {
  let component: DeleteTouristComponent;
  let fixture: ComponentFixture<DeleteTouristComponent>;
  let serviceSpy: jasmine.SpyObj<TouristService>;
  let routerSpy: jasmine.SpyObj<Router>;
  beforeEach(async () => {
    serviceSpy = jasmine.createSpyObj<TouristService>('TouristService', [
      'getById',
      'deleteById',
    ]);
    routerSpy = jasmine.createSpyObj<Router>('Router', ['navigate']);
    await TestBed.configureTestingModule({
      declarations: [DeleteTouristComponent],
      imports: [],
      providers: [
        { provide: Router, useValue: routerSpy },
        { provide: TouristService, useValue: serviceSpy },
        {
          provide: ActivatedRoute,
          useValue: {
            snapshot: { paramMap: convertToParamMap({ id: '1' }) },
          },
        },
      ]
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DeleteTouristComponent);
    component = fixture.componentInstance;
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
