/* eslint-disable no-empty-function */
/* eslint-disable no-unused-vars */
/* eslint-disable no-return-assign */
/* eslint-disable comma-dangle */
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Tourist } from '../../models/tourist.model';
import { TouristService } from '../../services/tourist.service';

@Component({
  selector: 'app-delete-tourist',
  templateUrl: './delete-tourist.component.html',
  styleUrls: ['./delete-tourist.component.css'],
})
export class DeleteTouristComponent implements OnInit {
  tourist!: Tourist;

  constructor(
    private touristService: TouristService,
    private route: ActivatedRoute,
    private router: Router
  ) {}

  ngOnInit(): void {
    this.loadData();
  }

  loadData() {
    const touristId = String(this.route.snapshot.paramMap.get('id'));
    this.touristService.getById(touristId).subscribe(
      (resp) => (this.tourist = resp),
      (error) => console.error(error)
    );
  }

  delete(id: number) {
    this.touristService.deleteById(id).subscribe(
      () => this.router.navigate(['turistas']),
      (error) => console.error(error)
    );
  }
}
