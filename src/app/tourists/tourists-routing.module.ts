import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AuthGuardService } from '../security/services/auth-guard.service';
import { DeleteTouristComponent } from './components/delete-tourist/delete-tourist.component';
import { TouristFormComponent } from './components/tourist-form/tourist-form.component';
import { TouristComponent } from './components/tourist/tourist.component';

const routes: Routes = [
  { path: '', component: TouristComponent, canActivate: [AuthGuardService] },
  { path: 'crear', component: TouristFormComponent, canActivate: [AuthGuardService] },
  { path: 'editar/:id', component: TouristFormComponent, canActivate: [AuthGuardService] },
  { path: 'eliminar/:id', component: DeleteTouristComponent, canActivate: [AuthGuardService] },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class TouristRoutingModule {}
