/* eslint-disable import/no-unresolved */
/* eslint-disable no-empty-function */
/* eslint-disable no-unused-vars */
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { Tourist } from '../models/tourist.model';

@Injectable({
  providedIn: 'root',
})
export class TouristService {
  private API_SERVER: string = `${environment.baseUrl}turistas`;

  constructor(private http: HttpClient) {}

  public getAll(): Observable<Tourist[]> {
    return this.http.get<Tourist[]>(this.API_SERVER);
  }

  public getById(id: string): Observable<Tourist> {
    return this.http.get<Tourist>(`${this.API_SERVER}/${id}`);
  }

  public save(tourist: Tourist): Observable<Tourist> {
    return this.http.post<Tourist>(this.API_SERVER, tourist);
  }

  public deleteById(id: number): Observable<string> {
    return this.http.delete<string>(`${this.API_SERVER}/delete/${id}`);
  }

  public update(tourist: Tourist): Observable<string> {
    return this.http.put<string>(`${this.API_SERVER}/update`, tourist);
  }
}
