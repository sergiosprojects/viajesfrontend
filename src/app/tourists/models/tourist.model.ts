export interface Tourist {
  id: number;
  names: string;
  surnames: string;
  idType: string;
  dateOfBirth: Date;
  travelFrequency: number;
}
