import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HttpClientModule } from '@angular/common/http';
import { TouristComponent } from './components/tourist/tourist.component';
import { TouristFormComponent } from './components/tourist-form/tourist-form.component';
import { TouristRoutingModule } from './tourists-routing.module';
import { TouristService } from './services/tourist.service';
import { SharedModule } from '../shared/shared.module';
import { DeleteTouristComponent } from './components/delete-tourist/delete-tourist.component';

@NgModule({
  declarations: [TouristComponent, TouristFormComponent, DeleteTouristComponent],
  imports: [CommonModule, HttpClientModule, TouristRoutingModule, SharedModule],
  exports: [],
  providers: [TouristService],
})
export class TouristsModule {}
